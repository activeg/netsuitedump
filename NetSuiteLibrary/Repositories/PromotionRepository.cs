﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using NetSuiteLibrary.com.netsuite.webservices;
using System.Net;
using MySql.Data.MySqlClient;
using Dapper;

namespace NetSuiteLibrary
{
    public class PromotionRepository : BaseRepository
    {
        public PromotionRepository() : base()
        {

        }

        #region Properties
        
        #endregion

        #region Methods

        public int GetPromotionCount()
        {
            int count = 0;
            if (_isAuthenticated)
            {
                CustomizationRef customRef = GetInternalId.GetID("customrecord_promo", _service);
                CustomRecord promo = new CustomRecord();
                CustomFieldRef[] fields = promo.customFieldList;
                CustomRecordSearch promoSearch = new CustomRecordSearch();

                CustomRecordSearchBasic promoSearchBasic = new CustomRecordSearchBasic();

                
                RecordRef refType = new RecordRef();
                refType.internalId = customRef.internalId;
                /*refType.name = customRef.name;
                refType.type = customRef.type;
                refType.typeSpecified = customRef.typeSpecified;*/
                promoSearchBasic.recType = refType;
                promo.recType = refType;
                promo.scriptId = "customrecord_promo";

                SearchBooleanCustomField filter1 = new SearchBooleanCustomField();
                filter1.scriptId = "custrecord_promo_deo_active";
                filter1.searchValue = true;
                filter1.searchValueSpecified = true;

                SearchMultiSelectCustomField filter2 = new SearchMultiSelectCustomField();
                filter2.scriptId = "custrecord_promo_client";
                ListOrRecordRef refval = new ListOrRecordRef();
                refval.name = "40 Kumho Tire USA Inc";
                refval.internalId = "365";
                refval.typeId = "-2";
                filter2.searchValue = new ListOrRecordRef[] { refval };
                filter2.@operator = SearchMultiSelectFieldOperator.anyOf;
                filter2.operatorSpecified = true;

                promoSearchBasic.customFieldList = new SearchCustomField[] { filter1, filter2 };

                promoSearch.basic = promoSearchBasic;
                
                SearchResult response = _service.search(promoSearch);
                if (response.status.isSuccess)
                {
                    count = response.totalRecords;
                }
                else
                {

                }
            }
            return count;
        }
        public ObservableCollection<CustomRecord> GetPromotions()
        {
            ObservableCollection<CustomRecord> promotions = new ObservableCollection<CustomRecord>();
            if (_isAuthenticated)
            {
                CustomizationRef customRef = GetInternalId.GetID("customrecord_promo", _service);
                CustomRecord promo = new CustomRecord();
                CustomFieldRef[] fields = promo.customFieldList;
                CustomRecordSearch promoSearch = new CustomRecordSearch();

                CustomRecordSearchBasic promoSearchBasic = new CustomRecordSearchBasic();


                RecordRef refType = new RecordRef();
                refType.internalId = customRef.internalId;
                /*refType.name = customRef.name;
                refType.type = customRef.type;
                refType.typeSpecified = customRef.typeSpecified;*/
                promoSearchBasic.recType = refType;
                promo.recType = refType;
                promo.scriptId = "customrecord_promo";

                SearchBooleanCustomField filter1 = new SearchBooleanCustomField();
                filter1.scriptId = "custrecord_promo_deo_active";
                filter1.searchValue = true;
                filter1.searchValueSpecified = true;
                /*
                SearchMultiSelectCustomField filter2 = new SearchMultiSelectCustomField();
                filter2.scriptId = "custrecord_promo_client";
                ListOrRecordRef refval = new ListOrRecordRef();
                refval.name = "40 Kumho Tire USA Inc";
                refval.internalId = "365";
                refval.typeId = "-2";
                filter2.searchValue = new ListOrRecordRef[] { refval };
                filter2.@operator = SearchMultiSelectFieldOperator.anyOf;
                filter2.operatorSpecified = true;
                */
                promoSearchBasic.customFieldList = new SearchCustomField[] { filter1 };

                promoSearch.basic = promoSearchBasic;

                SearchResult response = _service.search(promoSearch);
                if (response.status.isSuccess)
                {
                    if (response.totalRecords > 0)
                    {
                        for (int i = 1; i <= response.totalPages; i++)
                        {
                            promotions = new ObservableCollection<CustomRecord>(promotions.Union(ProcessPromotionPage(response)));
                            if (response.pageIndex < response.totalPages)
                            {
                                response = _service.searchMoreWithId(response.searchId, i + 1);
                            }
                        }
                    }
                }
            }
            return promotions;
        }
        private ObservableCollection<CustomRecord> ProcessPromotionPage(SearchResult response)
        {
            ObservableCollection<CustomRecord> promoPage = new ObservableCollection<CustomRecord>();
            Record[] records = response.recordList;
            CustomRecord promotion;
            for (int i = 0, j = (response.pageIndex - 1) * _pageSize; i < records.Length; i++, j++)
            {
                promotion = (CustomRecord)records[i];
                promoPage.Add(promotion);
            }
            return promoPage;
        }
        public void ProcessPromotions(CustomRecord promo)
        {
            using (MySqlConnection conn = OpenConnection())
            {
                string query = "select count(*) from promotions where ID = " + promo.internalId;
                int count = conn.Query<int>(query).FirstOrDefault();
                if (count > 0)
                {
                    UpdateCustomRecord(promo, "promotions");
                }
                else
                {
                    InsertCustomRecord(promo, "promotions");
                }
            }
        }
        #endregion
    }


    
}
