﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NetSuiteLibrary.com.netsuite.webservices;
using System.Data;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;
using Dapper;

namespace NetSuiteLibrary
{
    public class CompanyBrandRepository : BaseRepository
    {
        public CompanyBrandRepository() : base()
        {
        }

        

        public CustomRecord GetCompanyBrand(int ID)
        {
            CustomRecord record = new CustomRecord();

            return record;
        }

        //public ObservableCollection<CustomRecord> GetCompanyBrands()
        //{
        //    return GetCustomRecords("customrecord_company_brands");
        //}

        public void ProcessCompanyBrands(CustomRecord brand)
        {
            using (MySqlConnection conn = OpenConnection())
            {
                string query = "select count(*) from companybrands where ID = " + brand.internalId;
                int count = conn.Query<int>(query).FirstOrDefault();
                if(count > 0)
                {
                    UpdateCustomRecord(brand, "companybrands");
                }
                else
                {
                    InsertCustomRecord(brand, "companybrands");
                }
            }
        }

        
        
    }
}
