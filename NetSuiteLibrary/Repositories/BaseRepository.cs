﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using NetSuiteLibrary.com.netsuite.webservices;
using System.Net;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using MySql.Data.Entity;
using MySql.Data;
using MySql.Data.MySqlClient;
using Dapper;

namespace NetSuiteLibrary
{
    public class BaseRepository
    {
        public BaseRepository()
        {
            InitializeConnection();
        }
        #region Properties
        /// <value>Proxy class that abstracts the communication with the 
        /// NetSuite Web Services. All NetSuite operations are invoked
        /// as methods of this class.</value>
        protected NetSuiteService _service;

        /// <value>Flag that indicates whether the user is currently 
		/// authentciated, and therefore, whether a valid session is 
		/// available</value>
		protected bool _isAuthenticated;

        /// <value>Utility class for logging</value>
		//private Logger _out;

        /// <value>A NameValueCollection that abstracts name/value pairs from
        /// the app.config file in the Visual .NET project. This file is called
        /// [AssemblyName].exe.config in the distribution</value>
        protected System.Collections.Specialized.NameValueCollection _dataCollection;

        /// <value>Default page size used for search in this application</value>
        protected int _pageSize;

        protected List<string> columnNames = new List<string>();

        #endregion

        #region Methods
        private void InitializeConnection()
        {
            ServicePointManager.ServerCertificateValidationCallback += delegate (object sender, System.Security.Cryptography.X509Certificates.X509Certificate certificate, System.Security.Cryptography.X509Certificates.X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors)
            {
                return true;
            };


            
            _dataCollection = ConfigurationManager.AppSettings;

            // Instantiate the NetSuite web services
            try
            {
                _service = new DataCenterAwareNetSuiteService(_dataCollection["login.acct"]);
            }
            catch (Exception e)
            {
                Console.Write(e.Message);
            }
            if (!_isAuthenticated)
            {
                Login newLogin = new Login();
                _service = newLogin.go();
                _pageSize = newLogin._pageSize;
                _isAuthenticated = newLogin.IsAuthenticated;
            }
        }
        
        protected static MySqlConnection OpenConnection()
        {
            MySqlConnection connection = new MySqlConnection(new DBConnection().ConnectionString);
            try
            {
                connection.Open();
            }
            catch (Exception e)
            {
                Debug.WriteLine("Connection error: " + e.Message);
            }
            return connection;
        }

        #endregion
    }
}
