﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NetSuiteLibrary.com.netsuite.webservices;
using MySql.Data.MySqlClient;
using Dapper;
using System.Diagnostics;

namespace NetSuiteLibrary
{
    public class CustomRecordRepository : BaseRepository
    {
        public CustomRecordRepository() : base()
        {

        }
        private string netSuiteName;
        public string NetSuiteName
        {
            get { return netSuiteName; }
            set { netSuiteName = value; }
        }
        private string mySqlTableName;
        public string MySqlTableName
        {
            get { return mySqlTableName; }
            set { mySqlTableName = value; }
        }
        #region Methods

        public void GetCustomRecords()
        {
            GetCustomRecordsAndProcess(NetSuiteName);
        }
        private void GetCustomRecordsAndProcess(string customRecordScriptID)
        {
            TPRecordInfo recInfo = CustomerSpecific.RecInfo.Where(a => a.NetSuiteType == customRecordScriptID).FirstOrDefault();

            if (_isAuthenticated)
            {
                CustomizationRef customRef = GetInternalId.GetID(customRecordScriptID, _service);

                CustomRecordSearch recSearch = new CustomRecordSearch();

                CustomRecordSearchBasic recSearchBasic = new CustomRecordSearchBasic();
                RecordRef refType = new RecordRef();
                refType.internalId = customRef.internalId;

                recSearchBasic.recType = refType;
                Customer specific = CustomerSpecific.MyCustomer;
                if (specific != null)
                {
                    SearchMultiSelectCustomField custID = new SearchMultiSelectCustomField();

                    custID.scriptId = recInfo.ClientField;
                    custID.@operator = SearchMultiSelectFieldOperator.anyOf;
                    custID.operatorSpecified = true;
                    ListOrRecordRef search = new ListOrRecordRef();
                    search.internalId = CustomerSpecific.MyCustomer.internalId;
                    search.name = CustomerSpecific.MyCustomer.entityId;
                    search.typeId = "-2";
                    ListOrRecordRef[] searches = new ListOrRecordRef[] { search };
                    custID.searchValue = searches;

                    recSearchBasic.customFieldList = new SearchCustomField[] { custID };
                }

                if(CustomerSpecific.LastModifiedFilter)
                {
                    SearchDateField lastMod = new SearchDateField();
                    lastMod.@operator = SearchDateFieldOperator.onOrAfter;
                    lastMod.operatorSpecified = true;
                    lastMod.searchValue = DateTime.Today.AddDays((-1) * CustomerSpecific.LastModifiedDaysBack);
                    lastMod.searchValueSpecified = true;
                    recSearchBasic.lastModified = lastMod;
                }

                recSearch.basic = recSearchBasic;

                DateTime start = DateTime.Now;

                SearchResult response = _service.search(recSearch);
                if (response.status.isSuccess)
                {
                    SyncReports.AddQueried(customRecordScriptID, response.totalRecords);
                    if (response.totalRecords > 0)
                    {
                        int totalProcessed = 0;
                        for (int i = 1; i <= response.totalPages; i++)
                        {
                            //records = new ObservableCollection<CustomRecord>(records.Union(NetSuiteRecords.ProcessRecordPage(response, _pageSize)));
                            System.Threading.Tasks.Task.Run(() =>
                            {
                                int processed = 0;
                                string table = MySqlTableName;
                                ObservableCollection<CustomRecord> recordList = ProcessRecordPage(response, _pageSize);
                                foreach (CustomRecord record in recordList)
                                {
                                    ProcessCustomRecords(record, table);
                                    processed++;
                                }
                                SyncReports.AddProcessed(customRecordScriptID, processed);
                            });
                            totalProcessed += response.recordList.Count();
                            DateTime tempEnd = DateTime.Now;
                            string temptimetaken = tempEnd.Subtract(start).TotalSeconds.ToString();
                            Debug.WriteLine(customRecordScriptID + " - " + totalProcessed + " records. " + temptimetaken + " seconds.");
                            if (CustomerSpecific.ShouldLimit)
                            {
                                if (totalProcessed >= CustomerSpecific.LimitNumber)
                                {
                                    break;
                                }
                            }
                            if (response.pageIndex < response.totalPages)
                            {
                                response = _service.searchMoreWithId(response.searchId, i + 1);
                            }
                        }
                    }
                }
                else
                {
                    SyncReports.AddError(customRecordScriptID, response.status.statusDetail.FirstOrDefault().message);
                    Debug.WriteLine(response.status.statusDetail.FirstOrDefault().message);
                }
                DateTime end = DateTime.Now;
                TimeSpan timetaken = end.Subtract(start);
                SyncReports.AddElapsedTime(customRecordScriptID, timetaken);
            }
        }
        private ObservableCollection<CustomRecord> ProcessRecordPage(SearchResult response, int _pageSize)
        {
            ObservableCollection<CustomRecord> recPage = new ObservableCollection<CustomRecord>();
            Record[] records = response.recordList;
            CustomRecord record;
            for (int i = 0, j = (response.pageIndex - 1) * _pageSize; i < records.Length; i++, j++)
            {
                record = (CustomRecord)records[i];
                recPage.Add(record);
            }
            return recPage;
        }
        
        private void ProcessCustomRecords(CustomRecord record, string table)
        {
            using (MySqlConnection conn = OpenConnection())
            {
                string query = "select count(*) from " + table + " where ID = " + record.internalId;
                int count = conn.Query<int>(query).FirstOrDefault();
                if (count > 0)
                {
                    UpdateCustomRecord(record, table);
                }
                else
                {
                    InsertCustomRecord(record, table);
                }
            }
        }

        private void InsertCustomRecord(CustomRecord record, string tablename)
        {
            using (MySqlConnection conn = OpenConnection())
            {
                if (columnNames.Count == 0)
                {
                    string query = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA= 'netsuitetest' AND TABLE_NAME= '" + tablename + "'";
                    columnNames = conn.Query<string>(query).ToList();
                }

                string insert = "insert into " + tablename + " ";
                CustomFieldRef[] fieldList = record.customFieldList;
                List<PropertyDefinition> defs = new List<PropertyDefinition>();

                string fieldBuilder = "(ID, ";
                string valueBuilder = "(" + record.internalId + ", ";
                if (record.lastModifiedSpecified)
                {
                    fieldBuilder += "LastModifiedDate, ";
                    valueBuilder += "STR_TO_DATE('" + record.lastModified.ToString() + "', '%m/%d/%Y %r'), ";
                }
                if (fieldList != null)
                {
                    foreach (CustomFieldRef field in fieldList)
                    {
                        if (columnNames.Where(a => a == field.scriptId).Count() > 0)
                        {

                            string fieldName = field.scriptId;
                            fieldBuilder += fieldName + ", ";
                            string fieldValue = GetCustomRecord.GetValue(field);

                            valueBuilder += fieldValue + ", ";

                        }
                    }
                }
                string fieldsBuilt = fieldBuilder.Substring(0, fieldBuilder.Length - 2) + ")";
                string valueBuilt = valueBuilder.Substring(0, valueBuilder.Length - 2) + ")";

                insert += fieldsBuilt + " VALUES " + valueBuilt;

                conn.Execute(insert);
            }
        }

        private void UpdateCustomRecord(CustomRecord record, string tablename)
        {
            using (MySqlConnection conn = OpenConnection())
            {
                if (columnNames.Count == 0)
                {
                    string query = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA= 'netsuitetest' AND TABLE_NAME= '" + tablename + "'";
                    columnNames = conn.Query<string>(query).ToList();
                }

                string update = "update " + tablename + " set ";
                CustomFieldRef[] fieldList = record.customFieldList;
                List<PropertyDefinition> defs = new List<PropertyDefinition>();

                string fieldBuilder = "";

                if (record.lastModifiedSpecified)
                {
                    fieldBuilder += "LastModifiedDate = STR_TO_DATE('" + record.lastModified.ToString() + "', '%m/%d/%Y %r'), ";
                }
                if (fieldList != null)
                {
                    foreach (CustomFieldRef field in fieldList)
                    {
                        if (columnNames.Where(a => a == field.scriptId).Count() > 0)
                        {

                            string fieldName = field.scriptId;

                            string fieldValue = GetCustomRecord.GetValue(field);
                            fieldBuilder += fieldName + " = " + fieldValue + ", ";
                        }
                    }
                }
                if (fieldBuilder != null && fieldBuilder.Length > 5)
                {
                    string fieldsBuilt = fieldBuilder.Substring(0, fieldBuilder.Length - 2);

                    update += fieldsBuilt + " where ID = " + record.internalId;

                    conn.Execute(update);
                }
            }
        }

        #endregion
    }
}
