﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using NetSuiteLibrary.com.netsuite.webservices;
using System.Net;
using MySql.Data.MySqlClient;
using Dapper;
using System.Diagnostics;

namespace NetSuiteLibrary
{
    public class AddressRepository : BaseRepository
    {
        public AddressRepository() : base()
        {

        }
        #region Properties
        private string netSuiteName;
        public string NetSuiteName
        {
            get { return netSuiteName; }
            set { netSuiteName = value; }
        }
        private string mySqlTableName;
        public string MySqlTableName
        {
            get { return mySqlTableName; }
            set { mySqlTableName = value; }
        }
        #endregion

        public ObservableCollection<CustomerAddressbook> GetCustomerAddresses(Customer customer)
        {
            ObservableCollection<CustomerAddressbook> listOfAddresses = new ObservableCollection<CustomerAddressbook>();
            Address test = GetAddresses();
            if (customer.addressbookList != null)
            {
                CustomerAddressbook[] addrBook = customer.addressbookList.addressbook;
                var defaultShippingInternalID = "";
                foreach (CustomerAddressbook addr in addrBook)
                {
                    if (addrBook.Length == 1 || (addr.defaultShippingSpecified && addr.defaultShipping))
                    {
                        defaultShippingInternalID = addr.internalId;
                        listOfAddresses.Add(addr);
                    }
                    else { listOfAddresses.Add(addr); }
                }
            }
            return listOfAddresses;
        }
        public Address GetAddresses()
        {
            Address test = new Address();
            if (_isAuthenticated)
            {
                AddressSearchBasic addrSearch = new AddressSearchBasic();
                SearchStringField a1 = new SearchStringField();
                a1.@operator = SearchStringFieldOperator.@is;
                a1.operatorSpecified = true;
                a1.searchValue = "Midas International";
                addrSearch.addressee = a1;

                SearchResult response = _service.search(addrSearch);
                if (response.status.isSuccess)
                {
                    if (response.totalRecords > 0)
                    {
                        test = (Address)response.recordList.FirstOrDefault();
                    }
                }
            }
            return test;
        }
    }
}
