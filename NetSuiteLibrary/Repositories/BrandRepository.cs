﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using NetSuiteLibrary.com.netsuite.webservices;
using System.Net;

namespace NetSuiteLibrary
{
    public class BrandRepository : BaseRepository
    {
        public BrandRepository() : base()
        {

        }
        public int GetBrandCount()
        {
            int count = 0;
            if (_isAuthenticated)
            {
                CustomizationRef customRef = GetInternalId.GetID("customrecord_company_brands", _service);
                CustomRecord brand = new CustomRecord();
                CustomFieldRef[] fields = brand.customFieldList;
                CustomRecordSearch brandSearch = new CustomRecordSearch();

                CustomRecordSearchBasic brandSearchBasic = new CustomRecordSearchBasic();


                RecordRef refType = new RecordRef();
                refType.internalId = customRef.internalId;
                /*refType.name = customRef.name;
                refType.type = customRef.type;
                refType.typeSpecified = customRef.typeSpecified;*/
                brandSearchBasic.recType = refType;
                brand.recType = refType;
                brand.scriptId = "customrecord_company_brands";

                SearchBooleanCustomField filter1 = new SearchBooleanCustomField();
                filter1.scriptId = "custrecord_company_brands_test";
                filter1.searchValue = false;
                filter1.searchValueSpecified = true;

                brandSearchBasic.customFieldList = new SearchCustomField[] { filter1 };

                brandSearch.basic = brandSearchBasic;

                SearchResult response = _service.search(brandSearch);
                if (response.status.isSuccess)
                {
                    count = response.totalRecords;
                }
                else
                {
                    Console.WriteLine(response.status.statusDetail.FirstOrDefault().message);
                }
            }
            return count;
        }
    }
}
