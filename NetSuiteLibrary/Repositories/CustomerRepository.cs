﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using NetSuiteLibrary.com.netsuite.webservices;
using System.Net;
using MySql.Data.MySqlClient;
using Dapper;
using System.Diagnostics;

namespace NetSuiteLibrary
{
    public class CustomerRepository : BaseRepository
    {
        #region Constructors
        public CustomerRepository() : base()
        {
            
        }
        #endregion

        #region Properties
        private string netSuiteName;
        public string NetSuiteName
        {
            get { return netSuiteName; }
            set { netSuiteName = value; }
        }
        private string mySqlTableName;
        public string MySqlTableName
        {
            get { return mySqlTableName; }
            set { mySqlTableName = value; }
        }
        AddressRepository AddressRepo = new AddressRepository();
        #endregion

        #region Methods
        public ObservableCollection<Customer> GetCustomerRecords()
        {
            return GetCustomers();
        }

        public Customer GetCustomerEntityID(string entityId)
        {
            Customer result = new Customer();
            if (_isAuthenticated)
            {


                CustomerSearch custSearch = new CustomerSearch();
                CustomerSearchBasic custSearchBasic = new CustomerSearchBasic();

                SearchStringField entID = new SearchStringField();
                entID.@operator = SearchStringFieldOperator.@is;
                entID.operatorSpecified = true;
                entID.searchValue = entityId;

                custSearchBasic.entityId = entID;

                custSearch.basic = custSearchBasic;
                SearchResult cResult = _service.search(custSearch);
                result = (Customer)cResult.recordList.FirstOrDefault();
            }
            return result;
        }
        public Customer GetCustomer(long internalId)
        {
            Customer result = new Customer();
            if (_isAuthenticated)
            {
                

                CustomerSearch custSearch = new CustomerSearch();
                CustomerSearchBasic custSearchBasic = new CustomerSearchBasic();
                                
                SearchLongField intID = new SearchLongField();
                intID.@operator = SearchLongFieldOperator.equalTo;
                intID.operatorSpecified = true;
                intID.searchValue = internalId;
                intID.searchValueSpecified = true;
                
                custSearchBasic.internalIdNumber = intID;

                custSearch.basic = custSearchBasic;
                SearchResult cResult = _service.search(custSearch);
                result = (Customer)cResult.recordList.FirstOrDefault();
            }
            return result;
        }

        public ObservableCollection<Customer> GetCustomers()
        {
            ObservableCollection<Customer> customers = new ObservableCollection<Customer>();
            if (_isAuthenticated)
            {
                CustomerSearch custSearch = new CustomerSearch();
                CustomerSearchBasic custSearchBasic = new CustomerSearchBasic();
                /*
                SearchDateField modDate = new SearchDateField();
                modDate.@operator = SearchDateFieldOperator.on;
                modDate.operatorSpecified = true;
                modDate.searchValue = new DateTime(2015, 10, 21);
                modDate.searchValueSpecified = true;

                custSearchBasic.lastModifiedDate = modDate;*/
                /*
                SearchDateField saleDate = new SearchDateField();
                saleDate.@operator = SearchDateFieldOperator.on;
                saleDate.operatorSpecified = true;
                saleDate.searchValue = new DateTime(2014, 5, 19);
                saleDate.searchValueSpecified = true;
                custSearchBasic.firstSaleDate = saleDate;*/
                /*
                SearchLongField intID = new SearchLongField();
                intID.@operator = SearchLongFieldOperator.equalTo;
                intID.operatorSpecified = true;
                intID.searchValueSpecified = true;
                intID.searchValue = 47;
                custSearchBasic.internalIdNumber = intID;
                */
                //string specific = CustomerSpecific.GetSpecificCustomer();
                Customer specific = CustomerSpecific.MyCustomer;
                if (specific != null)
                {
                    /*SearchStringField fieldSearch = new SearchStringField();
                    fieldSearch.@operator = SearchStringFieldOperator.@is;
                    fieldSearch.operatorSpecified = true;
                    fieldSearch.searchValue = specific.entityId;
                    custSearchBasic.entityId = fieldSearch;*/
                    ProcessCustomerRecord(specific);
                    SearchMultiSelectCustomField cID = new SearchMultiSelectCustomField();
                    //SearchStringCustomField cID = new SearchStringCustomField();
                    cID.scriptId = "custentity_iscustomerof";
                    cID.@operator = SearchMultiSelectFieldOperator.anyOf;
                    cID.operatorSpecified = true;
                    ListOrRecordRef cRec = new ListOrRecordRef();
                    cRec.internalId = specific.internalId;
                    ListOrRecordRef[] lRec = new ListOrRecordRef[] { cRec };
                    cID.searchValue = lRec;
                    SearchCustomField[] fields = new SearchCustomField[] { cID };
                    custSearchBasic.customFieldList = fields;

                }
                if (CustomerSpecific.LastModifiedFilter)
                {
                    SearchDateField lastMod = new SearchDateField();
                    lastMod.@operator = SearchDateFieldOperator.onOrAfter;
                    lastMod.operatorSpecified = true;
                    lastMod.searchValue = DateTime.Today.AddDays((-1) * CustomerSpecific.LastModifiedDaysBack);
                    lastMod.searchValueSpecified = true;
                    custSearchBasic.lastModifiedDate = lastMod;
                }
                /*
                                SearchStringCustomField cID = new SearchStringCustomField();
                                cID.scriptId = "custentity_cli_main_company_brand";
                                cID.@operator = SearchStringFieldOperator.@is;
                                cID.operatorSpecified = true;
                                cID.searchValue = "Midas";
                                */

                custSearch.basic = custSearchBasic;
                DateTime start = DateTime.Now;
                

                SearchResult response = _service.search(custSearch);

                if (response.status.isSuccess)
                {
                    int totalProcessed = 0;
                    SyncReports.AddQueried("customer", response.totalRecords);
                    if (response.totalRecords > 0)
                    {
                        for (int i = 1; i <= response.totalPages; i++)
                        {
                            //customers = new ObservableCollection<Customer>(customers.Union(ProcessCustomerPage(response)));
                            totalProcessed += response.recordList.Count();
                            System.Threading.Tasks.Task.Run(() =>
                            {
                                int processed = 0;
                                ObservableCollection<Customer> recordList = ProcessCustomerPage(response);
                                foreach (Customer record in recordList)
                                {
                                    ProcessCustomerRecord(record);
                                    processed++;
                                }
                                SyncReports.AddProcessed("customer", processed);
                            });
                            DateTime tempEnd = DateTime.Now;
                            string temptimetaken = tempEnd.Subtract(start).TotalSeconds.ToString();
                            Debug.WriteLine("customer - " + totalProcessed + " records. " + temptimetaken + " seconds.");
                            if (CustomerSpecific.ShouldLimit)
                            {
                                if (totalProcessed >= CustomerSpecific.LimitNumber)
                                {
                                    break;
                                }
                            }
                            if (response.pageIndex < response.totalPages)
                            {
                                response = _service.searchMoreWithId(response.searchId, i + 1);
                            }
                        }
                        DateTime end = DateTime.Now;
                        TimeSpan timetaken = end.Subtract(start);
                        SyncReports.AddElapsedTime("customer", timetaken);
                    }
                }
            }
            return customers;
        }
        private ObservableCollection<Customer> ProcessCustomerPage(SearchResult response)
        {
            ObservableCollection<Customer> custPage = new ObservableCollection<Customer>();
            Record[] records = response.recordList;
            Customer customer;
            for (int i = 0, j=(response.pageIndex - 1) * _pageSize; i < records.Length; i++, j++)
            {
                customer = (Customer)records[i];
                custPage.Add(customer);
            }
            return custPage;
        }
        public SyncReport GetCustomerCount()
        {
            SyncReport report = new SyncReport();
            int count = 0;
            if (_isAuthenticated)
            {
                CustomerSearch custSearch = new CustomerSearch();
                CustomerSearchBasic custSearchBasic = new CustomerSearchBasic();
                /*
                SearchDateField modDate = new SearchDateField();
                modDate.@operator = SearchDateFieldOperator.after;
                modDate.operatorSpecified = true;
                modDate.searchValue = DateTime.Today.AddMonths(-1);
                modDate.searchValueSpecified = true;

                custSearchBasic.lastModifiedDate = modDate;

            */
                custSearch.basic = custSearchBasic;
                SearchResult response = _service.search(custSearch);
                count = response.totalRecords;
                if(!response.status.isSuccess)
                {
                    report.ErrorMessages.Add(response.status.statusDetail.FirstOrDefault().message);
                }
            }
            report.NumberQueried = count;
            report.MySqlTable = MySqlTableName;
            report.NetSuiteType = NetSuiteName;
            return report;
        }

        public void ProcessCustomerRecord(Customer record)
        {
            using (MySqlConnection conn = OpenConnection())
            {
                string query = "select count(*) from " + MySqlTableName + " where ID = " + record.internalId;
                int count = conn.Query<int>(query).FirstOrDefault();
                if (count > 0)
                {
                    UpdateCustomerRecord(record, MySqlTableName);
                }
                else
                {
                    InsertCustomerRecord(record, MySqlTableName);
                }
            }
        }
        private void UpdateCustomerRecord(Customer record, string tablename)
        {
            using (MySqlConnection conn = OpenConnection())
            {
                if (columnNames.Count == 0)
                {
                    string query = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA= 'netsuitetest' AND TABLE_NAME= '" + tablename + "'";
                    columnNames = conn.Query<string>(query).ToList();
                }

                string update = "update " + tablename + " set ";
                string fieldBuilder = "";

                var props = record.GetType().GetProperties();

                foreach (System.Reflection.PropertyInfo prop in props)
                {
                    PropertyDefinition propDef = new PropertyDefinition();

                    if (prop.Name == "customFieldList")
                    {
                        CustomFieldRef[] fields = record.customFieldList;
                        foreach (CustomFieldRef fieldRef in fields)
                        {
                            if (columnNames.Where(a => a == fieldRef.scriptId).Count() > 0)
                            {

                                string fieldName = fieldRef.scriptId;
                                string fieldValue = GetCustomRecord.GetValue(fieldRef);
                                fieldBuilder += fieldName + " = " + fieldValue + ", ";
                            }
                        }
                    }
                    if (columnNames.Where(a => a == prop.Name).Count() > 0)
                    {
                        
                        string toBuild = "";
                        var value = prop.GetValue(record, null);
                        if (value != null)
                        {
                            toBuild = GetStandardField(value, prop.GetMethod.ReturnType);
                        }
                        else
                        {
                            toBuild = "null";
                        }
                        fieldBuilder += prop.Name + " = " + toBuild + ", ";
                    }

                }

                string fieldsBuilt = fieldBuilder.Substring(0, fieldBuilder.Length - 2);

                update += fieldsBuilt + " where ID = " + record.internalId;

                conn.Execute(update);
            }
        }
        private void InsertCustomerRecord(Customer record, string tablename)
        {
            using (MySqlConnection conn = OpenConnection())
            {
                if (columnNames.Count == 0)
                {
                    string query = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA= 'netsuitetest' AND TABLE_NAME= '" + tablename + "'";
                    columnNames = conn.Query<string>(query).ToList();
                }

                string insert = "insert into " + tablename + " ";
                string fieldBuilder = "(ID, ";
                string valueBuilder = "(" + record.internalId + ", ";
                
                var props = record.GetType().GetProperties();

                foreach (System.Reflection.PropertyInfo prop in props)
                {
                    PropertyDefinition propDef = new PropertyDefinition();

                    if (prop.Name == "customFieldList")
                    {
                        CustomFieldRef[] fields = record.customFieldList;
                        foreach (CustomFieldRef fieldRef in fields)
                        {
                            if (columnNames.Where(a => a == fieldRef.scriptId).Count() > 0)
                            {

                                string fieldName = fieldRef.scriptId;
                                fieldBuilder += fieldName + ", ";
                                string fieldValue = GetCustomRecord.GetValue(fieldRef);

                                valueBuilder += fieldValue + ", ";

                            }
                        }
                    }
                    if (columnNames.Where(a => a == prop.Name).Count() > 0)
                    {
                        fieldBuilder += prop.Name + ", ";
                        string toBuild = "";
                        var value = prop.GetValue(record, null);
                        if (value != null)
                        {
                            toBuild = GetStandardField(value, prop.GetMethod.ReturnType);
                        }
                        else
                        {
                            toBuild = "null";
                        }
                        valueBuilder += toBuild + ", ";
                    }

                }

                string fieldsBuilt = fieldBuilder.Substring(0, fieldBuilder.Length - 2) + ")";
                string valueBuilt = valueBuilder.Substring(0, valueBuilder.Length - 2) + ")";

                insert += fieldsBuilt + " VALUES " + valueBuilt;

                conn.Execute(insert);
            }
        }

        private string GetStandardField(object value, Type type)
        {
            string formattedValue = value.ToString();
            switch(type.Name)
            {
                case "String":
                    formattedValue = "'" + value.ToString().Replace("'", "''") + "'";
                    break;
                case "Boolean":
                    bool val = (bool)value;
                    if(val)
                    { formattedValue = "1"; }
                    else
                    { formattedValue = "0"; }
                    break;
                case "Int64":
                case "Double":
                    break;
                case "DateTime":
                    formattedValue = "STR_TO_DATE('" + value + "', '%m/%d/%Y %r')";
                    break;
                case "RecordRef":
                    RecordRef myRef = (RecordRef)value;
                    if(CustomerSpecific.RecInfo.Any(a => a.NetSuiteType == myRef.type.ToString()))
                    {
                        formattedValue = myRef.internalId;
                    }
                    else
                    {
                        formattedValue = "'" + myRef.name.Replace("'", "''") + "'";
                    }
                    break;
                default:
                    formattedValue = "null";
                    break;
            }
            return formattedValue;
        }
        //CustomFieldRef[] customfieldList = record.customFieldList;
        //List<PropertyDefinition> defs = new List<PropertyDefinition>();


        //if (customfieldList != null)
        //{
        //    foreach (CustomFieldRef field in customfieldList)
        //    {
        //        if (columnNames.Where(a => a == field.scriptId).Count() > 0)
        //        {

        //            string fieldName = field.scriptId;
        //            fieldBuilder += fieldName + ", ";
        //            string fieldValue = GetCustomRecord.GetValue(field);

        //            valueBuilder += fieldValue + ", ";

        //        }
        //    }
        //}
        #endregion

    }
}
