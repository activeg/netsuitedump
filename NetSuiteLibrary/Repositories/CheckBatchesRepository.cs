﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NetSuiteLibrary.com.netsuite.webservices;
using System.Data;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;
using Dapper;

namespace NetSuiteLibrary
{
    public class CheckBatchesRepository : BaseRepository
    {
        public CheckBatchesRepository() : base()
        {

        }

        //public ObservableCollection<CustomRecord> GetCheckBatches()
        //{
        //    return GetCustomRecords("customrecord_ckbatches");
        //}

        public void ProcessCheckBatches(CustomRecord record)
        {
            string tableName = "checkbatches";
            using (MySqlConnection conn = OpenConnection())
            {
                string query = "select count(*) from " + tableName + " where ID = " + record.internalId;
                int count = conn.Query<int>(query).FirstOrDefault();
                if (count > 0)
                {
                    UpdateCustomRecord(record, tableName);
                }
                else
                {
                    InsertCustomRecord(record, tableName);
                }
            }
        }
    }

}
