﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetSuiteLibrary
{
    public class CustomRepository : BaseRepository
    {
        public CustomRepository() : base()
        {

        }
        public string ServiceInfo
        {
            get { return "test"; }
        }
        public int CustomCount
        {
            get { return 1; }
        }
        public int GetCustomCount()
        {
            
            return 1;
        }
    }
}
