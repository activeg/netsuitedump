﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NetSuiteLibrary.com.netsuite.webservices;

namespace NetSuiteLibrary
{
    public class SubmissionRepository : BaseRepository
    {
        public SubmissionRepository() : base()
        {

        }

        public ObservableCollection<TPSubmission> GetSubmissions()
        {
            ObservableCollection<TPSubmission> submissions = new ObservableCollection<TPSubmission>();

            if (_isAuthenticated)
            {
                CustomizationRef customRef = GetInternalId.GetID("customrecord_sub", _service);
                CustomRecord sub = new CustomRecord();
                CustomFieldRef[] fields = sub.customFieldList;
                CustomRecordSearch subSearch = new CustomRecordSearch();

                CustomRecordSearchBasic subSearchBasic = new CustomRecordSearchBasic();


                RecordRef refType = new RecordRef();
                refType.internalId = customRef.internalId;

                subSearchBasic.recType = refType;
                sub.recType = refType;
                sub.scriptId = "customrecord_sub";

                SearchBooleanCustomField filter1 = new SearchBooleanCustomField();
                filter1.scriptId = "custrecord_sub_opt_in";
                filter1.searchValue = true;
                filter1.searchValueSpecified = true;

                
                SearchMultiSelectCustomField filter2 = new SearchMultiSelectCustomField();
                filter2.scriptId = "custrecord_sub_client";
                ListOrRecordRef refval = new ListOrRecordRef();
                refval.name = "40 Kumho Tire USA Inc";
                refval.internalId = "365";
                refval.typeId = "-2";
                filter2.searchValue = new ListOrRecordRef[] { refval };
                filter2.@operator = SearchMultiSelectFieldOperator.anyOf;
                filter2.operatorSpecified = true;
                

                subSearchBasic.customFieldList = new SearchCustomField[] { filter1, filter2 };

                subSearch.basic = subSearchBasic;

                SearchResult response = _service.search(subSearch);
                if (response.status.isSuccess)
                {
                    if (response.totalRecords > 0)
                    {
                        for (int i = 1; i <= response.totalPages; i++)
                        {
                            submissions = new ObservableCollection<TPSubmission>(submissions.Union(ProcessSubmissionPage(response)));
                            if (response.pageIndex < response.totalPages)
                            {
                                response = _service.searchMoreWithId(response.searchId, i + 1);
                            }
                        }
                    }
                }

            }
            return submissions;
        }

        private ObservableCollection<TPSubmission> ProcessSubmissionPage(SearchResult response)
        {
            ObservableCollection<TPSubmission> recPage = new ObservableCollection<TPSubmission>();
            Record[] records = response.recordList;
            CustomRecord record;
            for (int i = 0, j = (response.pageIndex - 1) * _pageSize; i < records.Length; i++, j++)
            {
                record = (CustomRecord)records[i];
                recPage.Add(new TPSubmission(record));
            }
            return recPage;
        }
    }
}
