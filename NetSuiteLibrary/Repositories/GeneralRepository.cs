﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NetSuiteLibrary.com.netsuite.webservices;
using System.Data;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;
using Dapper;

namespace NetSuiteLibrary
{
    public class GeneralRepository : BaseRepository
    {

        public ObservableCollection<TPRecordInfo> GetRecInfo()
        {
            using (MySqlConnection conn = OpenConnection())
            {
                string query = "select * from typetablematches";
                return new ObservableCollection<TPRecordInfo>(conn.Query<TPRecordInfo>(query));
            }
        }

    }
}
