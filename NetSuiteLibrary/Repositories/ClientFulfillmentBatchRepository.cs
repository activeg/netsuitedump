﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NetSuiteLibrary.com.netsuite.webservices;
using MySql.Data.MySqlClient;
using Dapper;

namespace NetSuiteLibrary
{
    public class ClientFulfillmentBatchRepository : BaseRepository
    {
        public ClientFulfillmentBatchRepository() : base()
        {

        }

        private string NetSuiteName = "customrecord_cli_fulfillment_batches";
        private string tableName = "clientfulfillmentbatches";

        //public ObservableCollection<CustomRecord> GetClientProducts()
        //{
        //    return GetCustomRecords(NetSuiteName);
        //}

        public void ProcessCustomRecords(CustomRecord record)
        {
            using (MySqlConnection conn = OpenConnection())
            {
                string query = "select count(*) from " + tableName + " where ID = " + record.internalId;
                int count = conn.Query<int>(query).FirstOrDefault();
                if (count > 0)
                {
                    UpdateCustomRecord(record, tableName);
                }
                else
                {
                    InsertCustomRecord(record, tableName);
                }
            }
        }
    }
}
