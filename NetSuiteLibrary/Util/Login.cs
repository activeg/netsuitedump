﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NetSuiteLibrary.com.netsuite.webservices;
using System.Net;
using System.Configuration;

namespace NetSuiteLibrary
{
    class Login
    {
        public Login()
        {
            // In order to enable SOAPscope to work through SSL. Refer to FAQ for more details
            ServicePointManager.ServerCertificateValidationCallback += delegate (object sender, System.Security.Cryptography.X509Certificates.X509Certificate certificate, System.Security.Cryptography.X509Certificates.X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors)
            {
                return true;
            };
            _dataCollection = ConfigurationManager.AppSettings;

            // Instantiate the NetSuite web services
            try
            {
                _service = new DataCenterAwareNetSuiteService(_dataCollection["login.acct"]);
                setPreferences();
                _service.Timeout = 1000 * 60 * 60 * 2;
            }
            catch (Exception e)
            {
                Console.Write(e.Message);
            }

            
            //Enable cookie management
            //Uri myUri = new Uri("https://webservices.netsuite.com");
            //_service.CookieContainer = new CookieContainer();
        }

        /// <value>Proxy class that abstracts the communication with the 
        /// NetSuite Web Services. All NetSuite operations are invoked
        /// as methods of this class.</value>
        private NetSuiteService _service;
        private bool _isAuthenticated = false;
        public bool IsAuthenticated
        {
            get { return _isAuthenticated; }
        }
        public int _pageSize = 1000;
        /// <value>A NameValueCollection that abstracts name/value pairs from
        /// the app.config file in the Visual .NET project. This file is called
        /// [AssemblyName].exe.config in the distribution</value>
        private System.Collections.Specialized.NameValueCollection _dataCollection;

        /// Set up request level preferences as a SOAP header
        private Preferences _prefs;
        private SearchPreferences _searchPreferences;

        public NetSuiteService go()
        {

            if (!_isAuthenticated)
            {
                // Enable client cookie management. This is required.
                _service.CookieContainer = new CookieContainer();

                // Populate Passport object with all login information
                Passport passport = new Passport();
                RecordRef role = new RecordRef();

                passport.email = _dataCollection["login.email"];
                passport.password = _dataCollection["login.password"];
                role.internalId = _dataCollection["login.roleId"];
                passport.role = role;
                passport.account = _dataCollection["login.acct"];
                try
                {
                    SessionResponse resp = _service.login(passport);
                    Status status = resp.status;
                    // Process response
                    if (status.isSuccess == true)
                    {
                        _isAuthenticated = true;
                    }
                    else
                    {

                    }
                }
                catch (Exception e)
                {
                    Console.Write(e.Message);
                }



            }
            return _service;
        }

        private void setPreferences()
        {
            // Set up request level preferences as a SOAP header
            _prefs = new Preferences();
            // Preference to ask NS to treat all warnings as errors
            _prefs.warningAsErrorSpecified = true;
            _prefs.warningAsError = false;
            _prefs.ignoreReadOnlyFieldsSpecified = true;
            _prefs.ignoreReadOnlyFields = true;


            _service.preferences = _prefs;
            _searchPreferences = new SearchPreferences();

            // Setting this bodyFieldsOnly to true for faster search times on Opportunities
            _searchPreferences.bodyFieldsOnly = true;
            _searchPreferences.pageSize = _pageSize;
            _searchPreferences.pageSizeSpecified = true;

            _service.searchPreferences = _searchPreferences;

        }

    }
}
