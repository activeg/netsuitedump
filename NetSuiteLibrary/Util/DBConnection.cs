﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.Entity;
using MySql.Data;

namespace NetSuiteLibrary
{
    public class DBConnection
    {
        private string connString = "SERVER=localhost;DATABASE=netsuitetest;UID=tpuser;PASSWORD=tpuser;";

        public DBConnection()
        {
        }
        public string ConnectionString
        {
            get { return connString; }
            set { connString = value; }
        }
    }
}
