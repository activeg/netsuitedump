﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetSuiteLibrary
{
    public static class SyncReports
    {

        private static ObservableCollection<SyncReport> reports = new ObservableCollection<SyncReport>();
        public static ObservableCollection<SyncReport> Reports
        {
            get { return reports; }
            set { reports = value; }
        }
        public static void InitializeReports(ObservableCollection<TPRecordInfo> infos)
        {
            foreach(TPRecordInfo info in infos)
            {
                SyncReport report = new SyncReport();
                report.MySqlTable = info.MySqlTable;
                report.NetSuiteType = info.NetSuiteType;
                reports.Add(report);
            }
        }
        public static void AddQueried(string type, int amount)
        {
            SyncReport report = reports.Where(a => a.NetSuiteType == type).FirstOrDefault();
            report.NumberQueried = amount;
        }
        public static void AddProcessed(string type, int amount)
        {
            SyncReport report = reports.Where(a => a.NetSuiteType == type).FirstOrDefault();
            report.NumberProcessed += amount;
        }
        public static void AddError(string type, string error)
        {
            SyncReport report = reports.Where(a => a.NetSuiteType == type).FirstOrDefault();
            report.ErrorMessages.Add(error);
        }
        public static void AddElapsedTime(string type, TimeSpan time)
        {
            SyncReport report = reports.Where(a => a.NetSuiteType == type).FirstOrDefault();
            string elapsedtime = Math.Round(time.TotalSeconds, 2).ToString() + " seconds";
            if (time.TotalSeconds > 60 && time.TotalMinutes < 60)
            {
                elapsedtime = time.Minutes + " minutes and " + time.Seconds + " seconds";
            }
            if(time.TotalMinutes > 60)
            {
                elapsedtime = time.Hours + " hours, " + time.Minutes + " minutes and " + time.Seconds + " seconds";
            }
            report.ElapsedTime = elapsedtime;
        }
    }
}
