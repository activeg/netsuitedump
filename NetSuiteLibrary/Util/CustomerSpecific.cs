﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using NetSuiteLibrary.com.netsuite.webservices;

namespace NetSuiteLibrary
{
    public static class CustomerSpecific
    {
        private static Int32? id = null;
        public static Int32? ID {
            get
            {
                return id;
            }
            set
            {
                id = value;
            }
        }
        public static void Initialize()
        {
            GetSpecificCustomer();
            GetLimits();
            ModifiedSet();
        }
        private static ObservableCollection<TPRecordInfo> recInfo = new ObservableCollection<TPRecordInfo>();
        public static ObservableCollection<TPRecordInfo> RecInfo
        {
            get { return recInfo; }
            set { recInfo = value; }
        }

        public static void GetSpecificCustomer()
        {
            CustomerRepository CustomerRepo = new CustomerRepository();
            MyCustomer = CustomerRepo.GetCustomerEntityID(ConfigurationManager.AppSettings.Get("ClientEntityID"));
        }
        public static Customer MyCustomer { get; set; }

        public static void GetLimits()
        {
            ShouldLimit = Convert.ToBoolean(ConfigurationManager.AppSettings.Get("RestrictRecords"));
            if (ShouldLimit)
            {
                LimitNumber = Convert.ToInt32(ConfigurationManager.AppSettings.Get("RestrictTo"));
            }
            else
            {
                LimitNumber = 0;
            }
            NumberOfThreads = Convert.ToInt16(ConfigurationManager.AppSettings.Get("NumberOfThreads"));
        }
        public static void ModifiedSet()
        {
            LastModifiedFilter = Convert.ToBoolean(ConfigurationManager.AppSettings.Get("FilterLastModified"));
            LastModifiedDaysBack = Convert.ToInt32(ConfigurationManager.AppSettings.Get("LastModifiedDaysBack"));
        }

        public static bool ShouldLimit { get; set; }
        public static int LimitNumber { get; set; }
        public static bool LastModifiedFilter { get; set; }
        public static int LastModifiedDaysBack { get; set; }
        public static int NumberOfThreads { get; set; }
    }
}
