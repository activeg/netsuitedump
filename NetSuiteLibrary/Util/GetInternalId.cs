﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NetSuiteLibrary.com.netsuite.webservices;

namespace NetSuiteLibrary
{
    public class GetInternalId
    {
        public static CustomizationRef GetID(string scriptId, NetSuiteService _service)
        {
            // Perform getCustomizationId on custom record type
            CustomizationType ct = new CustomizationType();
            ct.getCustomizationTypeSpecified = true;
            ct.getCustomizationType = GetCustomizationType.customRecordType;

            // Retrieve active custom record type IDs. The includeInactives param is set to false.
            GetCustomizationIdResult getCustIdResult = _service.getCustomizationId(ct, false);

            foreach (var customizationRef in getCustIdResult.customizationRefList)
            {
                if (customizationRef.scriptId == scriptId) return customizationRef;
            }

            return null;
        }
    }
}
