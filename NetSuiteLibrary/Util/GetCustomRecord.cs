﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NetSuiteLibrary;
using NetSuiteLibrary.com.netsuite.webservices;

namespace NetSuiteLibrary
{
    public static class GetCustomRecord
    {
        public static List<string> internalTypes = new List<string>() { "-2", "32", "15", "28", "16", "35", "33", "8", "10", "12" };

        public static string GetValue(CustomFieldRef customField)
        {
            Type custom = customField.GetType();
            string returnValue = null;
            switch(custom.Name)
            {
                case "StringCustomFieldRef":
                    returnValue = "'" + ((StringCustomFieldRef)customField).value.Replace("'","''") + "'";
                    break;
                case "SelectCustomFieldRef":
                    SelectCustomFieldRef field = (SelectCustomFieldRef)customField;
                    if (internalTypes.Any(a => a == field.value.typeId))
                    {
                        returnValue = field.value.internalId;
                    }
                    else
                    {
                        returnValue = "'" + field.value.name.Replace("'", "''") + "'";
                    }
                    break;
                case "BooleanCustomFieldRef":
                    BooleanCustomFieldRef boolField = (BooleanCustomFieldRef)customField;
                    if(boolField.value)
                    {
                        returnValue = "1";
                    }
                    else
                    {
                        returnValue = "0";
                    }
                    break;
                case "DateCustomFieldRef":
                    returnValue = "STR_TO_DATE('" + ((DateCustomFieldRef)customField).value.ToString() + "', '%m/%d/%Y %r')";
                    break;
                case "MultiSelectCustomFieldRef":
                    MultiSelectCustomFieldRef mscfr = (MultiSelectCustomFieldRef)customField;
                    returnValue = "'" + mscfr.value.FirstOrDefault().name + "'";
                    break;
                case "DoubleCustomFieldRef":
                    DoubleCustomFieldRef doubleField = (DoubleCustomFieldRef)customField;
                    returnValue = doubleField.value.ToString();
                    break;
                case "LongCustomFieldRef":
                    LongCustomFieldRef longField = (LongCustomFieldRef)customField;
                    returnValue = longField.value.ToString();
                    break;
            }
            if(returnValue == null)
            {
                returnValue = "null";
            }
            return returnValue;
        }
    }
}
