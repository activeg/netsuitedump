﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetSuiteLibrary
{
    public class SyncReport
    {
        public SyncReport()
        {
            NumberProcessed = 0;
            NumberQueried = 0;
        }
        public string NetSuiteType { get; set; }
        public string MySqlTable { get; set; }
        public int NumberQueried { get; set; }
        public int NumberProcessed { get; set; }
        public string ElapsedTime { get; set; }
        private List<string> errorMessages = new List<string>();
        public List<string> ErrorMessages
        {
            get
            {
                return errorMessages;
            }
            set
            {
                errorMessages = value;
            }
        }
    }
}
