﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetSuiteLibrary
{
    public class LocalCustomer
    {
        #region Constructors
        public LocalCustomer()
        {

        }
        #endregion

        #region Properties

        public int InternalID { get; set; }

        public string Name { get; set; }

        #endregion
    }
}
