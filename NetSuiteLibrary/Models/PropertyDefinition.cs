﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetSuiteLibrary
{
    public class PropertyDefinition
    {
        public string RecordType { get; set; }
        public string PropertyName { get; set; }
        public Type ReturnType { get; set; }
        public string GetMethod { get; set; }
        public dynamic Value { get; set; }
    }
}
