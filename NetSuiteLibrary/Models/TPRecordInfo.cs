﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetSuiteLibrary
{
    public class TPRecordInfo
    {
        public Int32 ID { get; set; }
        public string NetSuiteType { get; set; }
        public string MySqlTable { get; set; }
        public int IsCustom { get; set; }
        public string ClientField { get; set; }
    }
}
