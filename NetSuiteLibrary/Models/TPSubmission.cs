﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NetSuiteLibrary.com.netsuite.webservices;

namespace NetSuiteLibrary
{
    public class TPSubmission
    {
        #region Constructors
        public TPSubmission(CustomRecord submission)
        {
            mySubmission = submission;
            //Client Name
            CustomFieldRef clRef = mySubmission.customFieldList.Where(a => a.scriptId == "custrecord_sub_client").FirstOrDefault();
            if (clRef != null)
            {
                SelectCustomFieldRef client = (SelectCustomFieldRef)clRef;
                clientName = client.value.name;
            }
        }
        #endregion


        #region
        private CustomRecord mySubmission;

        private string clientName;
        public string ClientName
        {
            get
            {
                return clientName;
            }
            set
            {
                clientName = value;
            }
        }


        #endregion
    }
}
