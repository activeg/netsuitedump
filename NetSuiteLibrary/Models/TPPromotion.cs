﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NetSuiteLibrary.com.netsuite.webservices;

namespace NetSuiteLibrary
{
    public class TPPromotion
    {
        #region Constructors

        public TPPromotion(CustomRecord promo)
        {
            myPromo = promo;

            //Promo Name
            promoName = myPromo.name;

            //Print Name
            CustomFieldRef pnRef = myPromo.customFieldList.Where(a => a.scriptId == "custrecord_promo_print_name").FirstOrDefault();
            if (pnRef != null)
            {
                StringCustomFieldRef client = (StringCustomFieldRef)pnRef;
                printName = client.value;
            }

            //Offer Code
            CustomFieldRef oRef = myPromo.customFieldList.Where(a => a.scriptId == "custrecord_promo_offer_code").FirstOrDefault();
            if (oRef != null)
            {
                StringCustomFieldRef prString = (StringCustomFieldRef)oRef;
                offerCode = prString.value;
            }
            //Promo Code
            CustomFieldRef pRef = myPromo.customFieldList.Where(a => a.scriptId == "custrecord_promo_promo_code").FirstOrDefault();
            if (pRef != null)
            {
                StringCustomFieldRef prString = (StringCustomFieldRef)pRef;
                promoCode = prString.value;
            }

            //Client Name
            CustomFieldRef clRef = myPromo.customFieldList.Where(a => a.scriptId == "custrecord_promo_client").FirstOrDefault();
            if (clRef != null)
            {
                SelectCustomFieldRef client = (SelectCustomFieldRef)clRef;
                clientName = client.value.name;
            }

            //Promo Brand
            CustomFieldRef brRef = myPromo.customFieldList.Where(a => a.scriptId == "custrecord_promo_brand").FirstOrDefault();
            if (brRef != null)
            {
                SelectCustomFieldRef brand = (SelectCustomFieldRef)brRef;
                promoBrand = brand.value.name;
            }
        }

        #endregion


        #region Properties
        private CustomRecord myPromo;

        private string promoBrand;
        public string PromoBrand
        {
            get
            {
                return promoBrand;
            }
            set
            {
                promoBrand = value;
            }
        }

        private string promoName;
        public string PromoName
        {
            get
            {
                return promoName;
            }
            set
            {
                promoName = value;
            }
        }
        private string printName;
        public string PrintName
        {
            get
            {
                return printName;
            }
            set
            {
                printName = value;
            }
        }

        private string promoCode;
        public string PromoCode
        {
            get
            {
                
                return promoCode;
            }
            set
            {
                promoCode = value;
            }
        }

        private string offerCode;
        public string OfferCode
        {
            get
            {

                return offerCode;
            }
            set
            {
                offerCode = value;
            }
        }

        private string clientName;
        public string ClientName {
            get
            {
                
                return clientName;
            }
            set
            {
                clientName = value;
            }
        }

        #endregion

    }
}
