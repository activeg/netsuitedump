﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Configuration;
using NetSuiteLibrary;
using NetSuiteLibrary.com.netsuite.webservices;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Forms;


namespace TestNetSuite
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        public MainWindow()
        {
            InitializeComponent();
            this.DataContext = this;
            //CustomerSpecific.ID = 1581750;

            worker.DoWork += worker_DoWork;
            worker.RunWorkerCompleted += worker_RunWorkerCompleted;
            worker.RunWorkerAsync();
            InitTimer();
            
        }
        private CustomerRepository CustomerRepo = new CustomerRepository();

        private CustomRecordRepository CustomRecordRepo = new CustomRecordRepository();

        private Timer timer1;
        public void InitTimer()
        {
            timer1 = new Timer();
            timer1.Tick += new EventHandler(timer1_Tick);
            timer1.Interval = 15000; // in miliseconds
            timer1.Start();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            Reports = new ObservableCollection<SyncReport>(SyncReports.Reports);
        }

        private void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            CustomerSpecific.Initialize();
            ObservableCollection<SyncReport> tempReport = new ObservableCollection<SyncReport>();
            GeneralRepository GeneralRepo = new GeneralRepository();
            ObservableCollection<TPRecordInfo> recInfo = GeneralRepo.GetRecInfo();
            SyncReports.InitializeReports(recInfo);
            Reports = new ObservableCollection<SyncReport>(SyncReports.Reports);
            int recsProcessed = 0;

            CustomerSpecific.RecInfo = recInfo;
            recInfo = new ObservableCollection<TPRecordInfo>(recInfo.Where(a => a.NetSuiteType == "customer"));
            DateTime start = DateTime.Now;
            foreach (TPRecordInfo info in recInfo)
            {
                Debug.WriteLine("Starting process for " + info.NetSuiteType);
                if (info.IsCustom == 1)
                {
                    SyncReport report = new SyncReport();
                    CustomRecordRepo.NetSuiteName = info.NetSuiteType;
                    CustomRecordRepo.MySqlTableName = info.MySqlTable;
                    CustomRecordRepo.GetCustomRecords();
                    /*foreach (CustomRecord record in recordList)
                    {
                        CustomRecordRepo.ProcessCustomRecords(record);
                        report.NumberProcessed++;
                    }*/
                }
                else
                {
                    if (info.NetSuiteType == "customer")
                    {
                        CustomerRepository CustomerRepo = new CustomerRepository();
                        CustomerRepo.NetSuiteName = info.NetSuiteType;
                        CustomerRepo.MySqlTableName = info.MySqlTable;
                        ObservableCollection<Customer> recordList = CustomerRepo.GetCustomerRecords();
                        
                    }
                }
                recsProcessed++;
                Reports = new ObservableCollection<SyncReport>(SyncReports.Reports);
            }
            DateTime end = DateTime.Now;
            TimeSpan diff = end.Subtract(start);
            Debug.WriteLine(diff.Minutes.ToString() + " minutes and " + diff.Seconds + " seconds to process");
        }
        private void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            System.Threading.Thread.Sleep(5);
            Reports = new ObservableCollection<SyncReport>(SyncReports.Reports);
        }
        private readonly BackgroundWorker worker = new BackgroundWorker();

        private ObservableCollection<SyncReport> reports = new ObservableCollection<SyncReport>();
        public ObservableCollection<SyncReport> Reports
        {
            get { return reports; }
            set
            {
                reports = value;
                RaisePropertyChanged("Reports");
            }
        }

        #region INPC Implementation
        public event PropertyChangedEventHandler PropertyChanged;
        public void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion INPC Implementation

    }
}
